type AriphmeticUnaryOperation = (x: number) => number;
type AriphmeticBinaryOperation = (x: number, y: number) => number;
type BaseSettings = {
  symbol: string,
  separator: string,
  decimal: string,
  formatWithSymbol: boolean,
  errorOnInvalid: boolean,
  precision: number,
  pattern: string,
  negativePattern: string,
  increment?: number,
  useVedic?: boolean,
};
type Settings = BaseSettings & {increment: number, groups: RegExp};
declare class currency {
  constructor(value: number | string | currency, opts: BaseSettings);
  intValue: number;
  value: number;
  _settings: Settings;
  _precision: number;
  add(this: currency, number: number): currency;
  subtract(number: number): currency;
  multiply(this: currency, number: number): currency;
  divide(this: currency, number: number): currency;
  distribute(this: currency, count: number): currency[];
  dollars(this: currency): number;
  cents(this: currency): number;
  format(this: currency, useSymbol: boolean): string;
  toString(this: currency): string;
  toJSON(this: currency): number;
}

const defaults: BaseSettings = {
    symbol: '$',
    separator: ',',
    decimal: '.',
    formatWithSymbol: false,
    errorOnInvalid: false,
    precision: 2,
    pattern: '!#',
    negativePattern: '-!#'
  };
  
  const round: AriphmeticUnaryOperation = v => Math.round(v);
  const pow: AriphmeticUnaryOperation = p => Math.pow(10, p);
  const rounding: AriphmeticBinaryOperation = (value, increment) => round(value / increment) * increment;
  
  const groupRegex = /(\d)(?=(\d{3})+\b)/g;
  const vedicRegex = /(\d)(?=(\d\d)+\d\b)/g;
  
  
  /**
   * Create a new instance of currency.js
   * @param {number|string|currency} value
   * @param {object} [opts]
   */
  function currency(this: currency | void, value: number | string | currency, opts: BaseSettings) {  
    if(!this || !(this instanceof currency)) {
      return new currency(value, opts);
    }
  
    let baseSettings = Object.assign({}, defaults, opts)
      , precision = pow(baseSettings.precision)
      , v = parse(value, baseSettings);
  
    this.intValue = v;
    this.value = v / precision;
  
    // Intended for internal usage only - subject to change
    this._settings = {
      ...baseSettings,
      increment: baseSettings.increment || (1 / precision),
      groups: baseSettings.useVedic ? vedicRegex : groupRegex,
    };
    this._precision = precision;
  }
  
  function parse(value: string | number | currency, opts: BaseSettings, useRounding = true): number {
    let v = 0
      , { decimal, errorOnInvalid, precision: decimals } = opts
      , precision = pow(decimals)
      , isNumber = typeof value === 'number';
  
    if (typeof value === 'number') {
      v = value * precision;
    }
    else if (typeof value !== 'string' && value instanceof currency) {
      v = value.value * precision;
    } else if (typeof value === 'string') {
      let regex = new RegExp('[^-\\d' + decimal + ']', 'g')
        , decimalString = new RegExp('\\' + decimal, 'g');
      v = Number(value
            .replace(/\((.*)\)/, '-$1')   // allow negative e.g. (1.99)
            .replace(regex, '')           // replace any non numeric values
            .replace(decimalString, '.'))  // convert any decimal values
            * precision;                  // scale number to integer value
      v = v || 0;
    } else {
      if(errorOnInvalid) {
        throw Error('Invalid Input');
      }
      v = 0;
    }
  
    // Handle additional decimal for proper rounding.
    v = Number(v.toFixed(4));
  
    return useRounding ? round(v) : v;
  }
  
  currency.prototype = {
  
    /**
     * Adds values together.
     * @param {number} number
     * @returns {currency}
     */
    add(this: currency, number: number): currency {
      let { intValue, _settings, _precision } = this;
      return new currency((intValue += parse(number, _settings)) / _precision, _settings);
    },
  
    /**
     * Subtracts value.
     * @param {number} number
     * @returns {currency}
     */
    subtract(number: number): currency {
      let { intValue, _settings, _precision } = this;
      return new currency((intValue -= parse(number, _settings)) / _precision, _settings);
    },
  
    /**
     * Multiplies values.
     * @param {number} number
     * @returns {currency}
     */
    multiply(this: currency, number: number): currency {
      let { intValue, _settings } = this;
      return new currency((intValue *= number) / pow(_settings.precision), _settings);
    },
  
    /**
     * Divides value.
     * @param {number} number
     * @returns {currency}
     */
    divide(this: currency, number: number): currency {
      let { intValue, _settings } = this;
      return new currency(intValue /= parse(number, _settings, false), _settings);
    },
  
    /**
     * Takes the currency amount and distributes the values evenly. Any extra pennies
     * left over from the distribution will be stacked onto the first set of entries.
     * @param {number} count
     * @returns {array}
     */
    distribute(this: currency, count: number): currency[] {
      let { intValue, _precision, _settings } = this
        , distribution = []
        , split = Math[intValue >= 0 ? 'floor' : 'ceil'](intValue / count)
        , pennies = Math.abs(intValue - (split * count));
  
      for (; count !== 0; count--) {
        let item = new currency(split / _precision, _settings);
  
        // Add any left over pennies
        pennies-- > 0 && (item = intValue >= 0 ? item.add(1 / _precision) : item.subtract(1 / _precision));
  
        distribution.push(item);
      }
  
      return distribution;
    },
  
    /**
     * Returns the dollar value.
     * @returns {number}
     */
    dollars(this: currency): number {
      return ~~this.value;
    },
  
    /**
     * Returns the cent value.
     * @returns {number}
     */
    cents(this: currency): number {
      let { intValue, _precision } = this;
      return ~~(intValue % _precision);
    },
  
    /**
     * Formats the value as a string according to the formatting settings.
     * @param {boolean} useSymbol - format with currency symbol
     * @returns {string}
     */
    format(this: currency, useSymbol: boolean): string {
      let { pattern, negativePattern, formatWithSymbol, symbol, separator, decimal, groups } = this._settings
        , values = (this + '').replace(/^-/, '').split('.')
        , dollars = values[0]
        , cents = values[1];
  
      // set symbol formatting
      typeof(useSymbol) === 'undefined' && (useSymbol = formatWithSymbol);
  
      return (this.value >= 0 ? pattern : negativePattern)
        .replace('!', useSymbol ? symbol : '')
        .replace('#', `${dollars.replace(groups, '$1' + separator)}${cents ? decimal + cents : ''}`);
    },
  
    /**
     * Formats the value as a string according to the formatting settings.
     * @returns {string}
     */
    toString(this: currency): string {
      let { intValue, _precision, _settings } = this;
      return rounding(intValue / _precision, _settings.increment).toFixed(_settings.precision);
    },
  
    /**
     * Value for JSON serialization.
     * @returns {float}
     */
    toJSON(this: currency): number {
      return this.value;
    }
  
  };
  
  export default currency;